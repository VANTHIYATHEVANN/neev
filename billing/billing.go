package billing

import "fmt"

type NetflixBillingService struct {
}

func NewBillingService() NetflixBillingService {
	return NetflixBillingService{}
}

func (b *NetflixBillingService) GenerateInvoice(amount int) {
	fmt.Println("---Invoice---")
	fmt.Printf("Total amount is %d", amount)
}
