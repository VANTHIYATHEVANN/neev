package subscription

type Status int

const (
	INACTIVE Status = 0
	ACTIVE   Status = 1
)
