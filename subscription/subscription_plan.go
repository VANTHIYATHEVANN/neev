package subscription

type Plan int

const (
	monthly Plan = 300
	yearly  Plan = 1000
)

func MonthlyPlan() Plan {
	return monthly
}

func YearlyPlan() Plan {
	return yearly
}
