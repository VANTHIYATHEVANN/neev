package subscription

import (
	"netflix/billing"
	"netflix/payment"
)

type Subscription struct {
	plan    Plan
	bill    billing.NetflixBillingService
	payment payment.Card
}

func NewSubscription(plan Plan) Subscription {
	return Subscription{
		plan: plan,
	}
}

func (s *Subscription) Activate() {
	// todo - write code to generate invoice & activate subscription based on Payment status
}
