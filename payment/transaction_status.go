package payment

type TransactionStatus int

const (
	FAILURE TransactionStatus = 0
	SUCCESS TransactionStatus = 1
)
