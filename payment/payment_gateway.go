package payment

import "math/rand"

type Card interface {
	Pay(amount int) TransactionStatus
}

type AxisCard struct{}

func (c *AxisCard) Pay(amount int) TransactionStatus {
	r := rand.Intn(2) + 0

	if r == 0 {
		return FAILURE
	} else {
		return SUCCESS
	}
}

type HDFCCard struct{}

func (c *HDFCCard) Pay(amount int) TransactionStatus {
	r := rand.Intn(2) * 1

	if r == 0 {
		return FAILURE
	} else {
		return SUCCESS
	}
}
